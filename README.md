# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* The initial set up for a testing framework for JavaScript
* Version 0.1


### How do I get set up? ###

* Summary of set up
* _Configuration_

      npm init

      npm install mocha chai --save-dev

* _Dependencies_

      node.js is already installed

* _Database configuration_
* _How to run tests_
* _Deployment instructions_

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Originally written by Alwin Solanky

### Mocha and Chai

    [Mocha](https://mochajs.org/) is Framework for testing JavaScript
    [Chai](http://chaijs.com/) is an assertion library

### Why Test?

Testing is a very good idea for building a large product or maintaining one as it will help to reduce cost and make for better more robust code in the long run.

Note that node.js has an assertion library by default, however it is not as powerful as Chai.

By default mocha looks for a folder called test.

_module.exports_ at the start of your _app.js_ means that what is put there is accessible outside of the file.

Using the default assert in the node.js library all you need is

      const assert = ('assert');

in the test script.

To run tests

    npm run test
    npm run test -s
    
