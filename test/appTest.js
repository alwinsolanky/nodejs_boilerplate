const assert = require('chai').assert;
const app = require('../app');

//Results
sayHiResult = app.sayHi();
addNumbersResults = app.addNumbers(5, 1);

describe('App', function(){

  describe('sayHi', function() {

    it('sayHi should return hi!', function(){
      assert.equal(sayHiResult, 'hi!');
    });

    it('sayHi should return the type string', function(){
      assert.typeOf(sayHiResult, 'string');
    });

  });

  describe('addNumbers()', function(){

    it('addNumbers should be above 5', function(){
      assert.isAbove(addNumbersResults, 5);
    });

    it('addNumbers should return the type number', function(){
      assert.typeOf(addNumbersResults, 'number');
    });


  });

});
